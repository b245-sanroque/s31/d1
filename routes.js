const http = require('http');
// use const to avoid reassigning value

const port = 4000;

const server = http.createServer((request, response) => {

	// Accessing the "greeting" route returns a message of "Hello Worl"

	if(request.url == '/greeting') {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Hello World!");
	} else if(request.url == '/homepage') {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Welcome to the homepage!");
	// } else{
	// 	// Set a status code for the response - a 404 means "NOT FOUND"
	// 	response.writeHead(404, {'Content-Type': 'text/plain'});
	// 	response.end("Page NOT Available!");
	}
});

server.listen(port);

console.log(`Server now accessible at localhost: ${port}`);


// c/p sa gitbash > npm install -g nodemon
// copy/paste sa gitbash > nodemon routes.js
	// to execute nodemon

// nodemon - package that allows automatic updates on our server

// error >> Error: listen EADDRINUSE: address already in use :::4000
// npx kill-port [port-number] >> to kill/close/terminate the server to check if it is already in use

// ctrl+c to stop nodemon, then kill-port