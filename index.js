// Use the "require" directive to load Node.js modules

// A "module" is a software component or part of a program that contains one or more routines.

// "http" module lets Node.js transfer data using the (HTTP) Hyper Text Transfer Protocol 


let http = require("http");

http.createServer(function (req, res) {

		// Use the writeHead() method to:
		// Set a status code for the response, a 200 means OK
		// Set the content-type of the response as a plain text message
	res.writeHead(200, {'Content-Type': 'text/plain'});
	res.end("Hello, B245!");

}).listen(4000);

// When server is running, console will print the message.
console.log('Server running at localhost:4000');

/*
	createServer 	- method, accepts a function.. 
					- two arguments (request, response)

	listen - 
	4000 - port - to establish connection

	200 - status code, for the response.. 
*/

/*
	HTTP response status codes

	 - HTTP response status codes indicate whether a specific HTTP request has been successfully completed. 

	 - Responses are grouped in five classes:
		- Informational responses (100 – 199)
		- Successful responses (200 – 299)
		- Redirection messages (300 – 399)
		- Client error responses (400 – 499)
		- Server error responses (500 – 599)
*/